<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419145405 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (1, 1, 7);');
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (2, 2, 7);');
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (3, 3, 7);');
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (4, 4, 7);');
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (5, 5, 7);');
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (6, 6, 6);');
        $this->addSql('INSERT INTO `configuration_slot` (`id`, `day_of_week`, `number_of_slot`) VALUES (7, 7, 7);');
        $this->addSql('INSERT INTO `configuration_company` (`id`, `name`, `only_one_booking_per_week`, `no_booking_past_date`, `can_book_same_day`) VALUES
(1, "Hooly", 1, 1, 0);');
        $this->addSql("INSERT INTO `foodtruck` (`id`, `email`, `roles`, `password`, `api_token`, `api_token_expires_at`) VALUES
(1, 'foodtruck@hooly.com', '[]', '$2y$13$z6mQv3FmdRHg.Wr5BO6/O.yaXEouJy5kqsAR5ArQc3X5lT5KzBGUC', 'f309a95f51244fb56e044681090bee1d', '2022-04-20 01:25:44');");
        $this->addSql("INSERT INTO `foodtruck` (`id`, `email`, `roles`, `password`, `api_token`, `api_token_expires_at`) VALUES
(2, 'foodtruck2@hooly.com', '[]', '$2y$13$z6mQv3FmdRHg.Wr5BO6/O.yaXEouJy5kqsAR5ArQc3X5lT5KzBGUC', NULL, NULL);");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('TRUNCATE `configuration_slot`');
        $this->addSql('TRUNCATE `configuration_company`');
        $this->addSql('TRUNCATE `foodtruck`');
    }
}
