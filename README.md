# Hooly - Foodtruck booking system

## Installation
- install docker & docker-compose on the system
- open a terminal and go into the docker directory
- type : `docker-compose up -d` 
  - to start the containers with PHP / MYSQL / NGINX
- open the docker container with
    - `docker exec -it app-php /bin/sh`
    - type: `composer install`
    - then : `symfony console doctrine:migrations:migrate` to create the DB
    - (if symfony is not install, follow the 3 steps from symfony.com to do it)
- the API should be available at `http://localhost:8001`

## Features
- to create a foodtruck, open : `http://localhost:8001/register`

---

**The angular front end should be able to login & use the API**

