<?php

namespace App\EventListener;


use App\Entity\Foodtruck;
use App\Repository\FoodtruckRepository;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;


class LogoutListener
{
    /**
     * LogoutListener constructor.
     * @param FlashBagInterface $flashBag
     * @param FoodtruckRepository $foodtruckRepository
     */
    public function __construct(
        private FlashBagInterface $flashBag,
        private FoodtruckRepository $foodtruckRepository
    ) {}

    /**
     * @param LogoutEvent $event
     * @param UserInterface $foodtruck
     * @return mixed
     */
    public function __invoke(LogoutEvent $event)
    {
        /** @var Foodtruck $user */
        $user = $event->getToken()->getUser();
        $foodtruck = $this->foodtruckRepository->getFoodtruckForToken($user->getApiToken());
        if (null !== $foodtruck) {
            assert($foodtruck instanceof Foodtruck);
            try {
                $this->foodtruckRepository->removeToken($foodtruck);
            } catch (\Exception $e) {
                $this->flashBag->add('success', 'Déconnexion réussie!');
            }
        }
        $this->flashBag->add('success', 'Déconnexion réussie!');
        return true;
    }
}
