<?php

namespace App\Security;

use App\Repository\FoodtruckRepository;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class HoolyAuthenticator extends AbstractAuthenticator
{
    public function __construct(
        private FoodtruckRepository $foodtruckRepository,
        private RouterInterface $router
    ) {}

    public function supports(Request $request): ?bool
    {
        if (($request->getPathInfo() === '/api/login' && $request->isMethod('POST'))
            ||
            ($request->getPathInfo() === '/register')
        ) {
            return false;
        }
        if (!$request->headers->has('X-AUTH-TOKEN')) {
            return true;
        }
        return $request->headers->has('X-AUTH-TOKEN');
    }

    public function authenticate(Request $request): Passport
    {
        $token = $request->headers->get('X-AUTH-TOKEN');
        if (!$token) {
            throw new CustomUserMessageAuthenticationException('No API Token');
        }
        return new SelfValidatingPassport(   new UserBadge($token, function() use ($token) {
            $foodtruck = $this->foodtruckRepository->getFoodtruckForToken($token);
            if (null === $foodtruck) {
                return false;
            }
            if ($foodtruck->getApiTokenExpiresAt() < new DateTime('now')) {
                return false;
            }
            return $foodtruck;
        }));
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

//    public function start(Request $request, AuthenticationException $authException = null): Response
//    {
//        /*
//         * If you would like this class to control what happens when an anonymous user accesses a
//         * protected page (e.g. redirect to /login), uncomment this method and make this class
//         * implement Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface.
//         *
//         * For more details, see https://symfony.com/doc/current/security/experimental_authenticators.html#configuring-the-authentication-entry-point
//         */
//    }
}
