<?php

namespace App\Helper;

/**
 * Acts like a DTO
 * Class BookingHelper
 * @package App\Helper
 */
final class BookingHelper
{
    public function __construct(
      private int|null $id,
      private string $date,
    ) {}

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(int|null $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

}
