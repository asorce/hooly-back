<?php
namespace App\Helper;

/**
 * Helper class to act as a DTO to send data to the front-end
 * Class FoodtruckHelper
 * @package App\Helper
 */
final class FoodtruckHelper {
    public function __construct(
        private string $email,
        private string $token,
    ) {}

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }


}
