<?php

namespace App\Helper;

/**
 * Acts like a DTO
 * Class SlotHelper
 * @package App\Helper
 */
final class SlotHelper {
    public function __construct(
        private string $date,
        private string $numberOfSlot,
        private array $bookings
    ) {}

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getNumberOfSlot(): string
    {
        return $this->numberOfSlot;
    }

    /**
     * @param string $numberOfSlot
     */
    public function setNumberOfSlot(string $numberOfSlot): void
    {
        $this->numberOfSlot = $numberOfSlot;
    }

    /**
     * @return array
     */
    public function getBookings(): array
    {
        return $this->bookings;
    }

    /**
     * @param array $bookings
     */
    public function setBookings(array $bookings): void
    {
        $this->bookings = $bookings;
    }

}
