<?php
namespace App\Service;

use App\Entity\Booking;
use App\Entity\Foodtruck;
use App\Helper\BookingHelper;
use App\Repository\BookingRepository;
use App\Repository\FoodtruckRepository;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BookingService
{
    public function __construct(
       private BookingRepository $bookingRepository,
       private FoodtruckRepository $foodtruckRepository,
       private ConfigurationCompanyService $configurationCompanyService,
       private ValidatorInterface $validator
    ) {}

    /**
     * @param Foodtruck $foodtruck
     * @param string $start
     * @param string $end
     * @return array
     */
    public function getBookingsForWeek(Foodtruck $foodtruck, string $start, string $end): array
    {
        $bookingsForWeek = [];
        $bookings = $this->bookingRepository->findBookingsForWeek($start, $end);
        foreach($bookings as $booking)
        {
            assert($booking instanceof Booking);
            $ownBooking = false;
            if ($booking->getFoodtruck()->getId() === $foodtruck->getId()) {
                $ownBooking = true;
            }
            $bookingHelper = new BookingHelper(
                $ownBooking ? $booking->getId() : null,
                $booking->getDate()->format('Y-m-d'),
            );
            $bookingsForWeek[] = $bookingHelper;
        }
        return $bookingsForWeek;
    }

    /**
     * @param Foodtruck $foodtruck
     * @return array
     */
    public function getBookingsForFoodtruck(Foodtruck $foodtruck): array
    {
        $bookings = $foodtruck->getBookings()->toArray();
        $bookingsHelper = [];
        foreach($bookings as $booking)
        {
            assert($booking instanceof Booking);
            $bookingHelper = new BookingHelper(
                $booking->getId(),
                $booking->getDate()->format('Y-m-d'),
            );
            $bookingsHelper[] = $bookingHelper;
        }
        return $bookingsHelper;
    }

    /**
     * Error codes could be returned to explain the reason of the error
     * -> return false is all cases is enough for the scope
     *
     * @param Foodtruck $foodtruck
     * @param string $date
     * @return bool
     */
    public function createBooking(Foodtruck $foodtruck, string $date): bool
    {
        $configurationCompany = $this->configurationCompanyService->getConfigurationCompany();
        if (!$configurationCompany) {
            return false;
        }

        if ($configurationCompany->getOnlyOneBookingPerWeek()) {
            $hasBookings = $this->bookingRepository->hasBookingAlready($foodtruck, $date);
            if ($hasBookings) {
                return false;
            }
        }

        if (!$configurationCompany->getCanBookSameDay() && $date === date('Y-m-d')) {
            return false;
        }

        if ($configurationCompany->getNoBookingPastDate() && $date < date('Y-m-d')) {
            return false;
        }

        $booking = new Booking();
        try {
            $booking->setDate(new DateTime($date));
        } catch (\Exception $e) {
            return false;
        }
        $booking->setFoodtruck($foodtruck);

        $errors = $this->validator->validate($booking);
        if (count($errors) > 0) {
            return false;
        }

        try {
            $this->bookingRepository->add($booking);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function deleteBooking(Foodtruck $foodtruck, int $bookingId): bool
    {
        $booking = $this->bookingRepository->findBookingByIdForFoodtruck($foodtruck, $bookingId);
        if (!$booking) {
            return false;
        }
        try {
            $this->bookingRepository->remove($booking);
        } catch (OptimisticLockException $e) {
            return false;
        }
        return true;
    }
}
