<?php
namespace App\Service;

use App\Helper\BookingHelper;
use App\Helper\SlotHelper;
use App\Repository\ConfigurationCompanyRepository;
use App\Repository\ConfigurationSlotRepository;

class SlotService
{
    public function __construct(
        private ConfigurationCompanyRepository $configurationCompanyRepository,
        private ConfigurationSlotRepository $configurationSlotRepository
    ) {}

    public function getSlotsForWeek(?int $week, ?int $year): array {
        if ($week && $year) {
            $dateTime = new \DateTime();
            $dateTime->setISODate($year, $week);
            $monday = $dateTime->format('Y-m-d');
            $dateTime->modify('+7 days');
            $sunday = $dateTime->format('Y-m-d');
        } else {
            $dateTime = new \DateTime('monday this week');
            $monday = $dateTime->format('Y-m-d');
            $dateTime->modify('+7 days');
            $sunday = $dateTime->format('Y-m-d');
        }

        $period = new \DatePeriod(
            new \DateTime($monday),
            new \DateInterval('P1D'),
            new \DateTime($sunday)
        );

        $configSlots = $this->configurationSlotRepository->findAll();
        $slotsByDay = [];
        foreach($configSlots as $config) {
            $slotsByDay[$config->getDayOfWeek()] = $config->getNumberOfSlot();
        }
        $slots = [];
        foreach($period as $date) {
            $dateFormatted = $date->format('Y-m-d');
            $dayofweek = date('w', strtotime($dateFormatted)) + 1;
            $slots[$dateFormatted] = $slotsByDay[$dayofweek];
        }
        return $slots;
    }

    /**
     * @param $slots
     * @param $bookings
     * @return array
     */
    public function computeSlotsAndBookingsForWeek($slots, $bookings): array
    {
        $slotsHelper = [];
        foreach($slots as $date => $numberOfSlot) {
            $bookingsSameDay = [];
            foreach($bookings as $booking) {
                assert($booking instanceof BookingHelper);
                if ($booking->getDate() === $date) {
                    $bookingsSameDay[] = $booking;
                }
            }
            $slotHelper = new SlotHelper(
              $date,
              $numberOfSlot,
              $bookingsSameDay
            );
            $slotsHelper[] = $slotHelper;
        }
        return $slotsHelper;
    }
}
