<?php

namespace App\Service;

use App\Entity\ConfigurationCompany;
use App\Repository\ConfigurationCompanyRepository;

class ConfigurationCompanyService
{
    public function __construct(
        private ConfigurationCompanyRepository $configurationCompanyRepository
    ) {}

    /**
     * There is for now only one company
     * -> The settings could've been stored in a config file as well
     * -> arbitrary choice to store it in the DB and query the company here with id 1
     * @return ConfigurationCompany
     */
    public function getConfigurationCompany(): ConfigurationCompany
    {
        return $this->configurationCompanyRepository->findOneBy(['id' => 1]);
    }
}
