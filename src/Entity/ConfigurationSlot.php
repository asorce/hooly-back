<?php

namespace App\Entity;

use App\Repository\ConfigurationSlotRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConfigurationSlotRepository::class)]
class ConfigurationSlot
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $dayOfWeek;

    #[ORM\Column(type: 'integer')]
    private $numberOfSlot;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDayOfWeek(): ?int
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(int $dayOfWeek): self
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getNumberOfSlot(): ?int
    {
        return $this->numberOfSlot;
    }

    public function setNumberOfSlot(int $numberOfSlot): self
    {
        $this->numberOfSlot = $numberOfSlot;

        return $this;
    }
}
