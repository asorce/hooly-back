<?php

namespace App\Entity;

use App\Repository\BookingRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: BookingRepository::class)]
class Booking
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\ManyToOne(targetEntity: Foodtruck::class, inversedBy: 'bookings')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Foodtruck $foodtruck;

    #[ORM\Column(type: 'date')]
    #[Assert\NotBlank]
    #[Assert\NotNull]
    #[Assert\Type('DateTimeInterface')]
    private ?DateTimeInterface $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFoodtruck(): ?Foodtruck
    {
        return $this->foodtruck;
    }

    public function setFoodtruck(?Foodtruck $foodtruck): self
    {
        $this->foodtruck = $foodtruck;

        return $this;
    }

    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
