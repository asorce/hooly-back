<?php

namespace App\Entity;

use App\Repository\ConfigurationCompanyRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ConfigurationCompanyRepository::class)]
class ConfigurationCompany
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'boolean')]
    private $onlyOneBookingPerWeek;

    #[ORM\Column(type: 'boolean')]
    private $noBookingPastDate;

    #[ORM\Column(type: 'boolean')]
    private $canBookSameDay;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getOnlyOneBookingPerWeek(): ?bool
    {
        return $this->onlyOneBookingPerWeek;
    }

    public function setOnlyOneBookingPerWeek(bool $onlyOneBookingPerWeek): self
    {
        $this->onlyOneBookingPerWeek = $onlyOneBookingPerWeek;

        return $this;
    }

    public function getNoBookingPastDate(): ?bool
    {
        return $this->noBookingPastDate;
    }

    public function setNoBookingPastDate(bool $noBookingPastDate): self
    {
        $this->noBookingPastDate = $noBookingPastDate;

        return $this;
    }

    public function getCanBookSameDay(): ?bool
    {
        return $this->canBookSameDay;
    }

    public function setCanBookSameDay(bool $canBookSameDay): self
    {
        $this->canBookSameDay = $canBookSameDay;

        return $this;
    }
}
