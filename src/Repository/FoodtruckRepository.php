<?php

namespace App\Repository;

use App\Entity\Foodtruck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method Foodtruck|null find($id, $lockMode = null, $lockVersion = null)
 * @method Foodtruck|null findOneBy(array $criteria, array $orderBy = null)
 * @method Foodtruck[]    findAll()
 * @method Foodtruck[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FoodtruckRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Foodtruck::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Foodtruck $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Foodtruck $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof Foodtruck) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @param $token
     * @return Foodtruck|null
     */
    public function getFoodtruckForToken($token): ?Foodtruck {
        $foodTruck = $this->findOneBy(['apiToken' => $token]);
        if ($foodTruck && !$foodTruck->isApiTokenExpired()) {
            return $foodTruck;
        }
        return null;
    }


    /**
     * @param $email
     * @return Foodtruck|null
     */
    public function findByEmail($email): ?Foodtruck {
        $foodTruck = $this->findOneBy(['email' => $email]);
        if ($foodTruck && !$foodTruck->isApiTokenExpired()) {
            return $foodTruck;
        }
        return null;
    }



    /**
     * @param Foodtruck $foodtruck
     * @throws Exception
     */
    public function createToken(Foodtruck $foodtruck): void {
        $foodtruck->createToken();
        try {
            $this->_em->persist($foodtruck);
            $this->_em->flush();
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    /**
     * @param Foodtruck $foodtruck
     * @throws Exception
     */
    public function removeToken(Foodtruck $foodtruck): void {
        $foodtruck->removeToken();
        try {
            $this->_em->persist($foodtruck);
            $this->_em->flush();
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
