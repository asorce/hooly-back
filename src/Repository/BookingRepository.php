<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\Foodtruck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Booking $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Booking $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @param string $start
     * @param string $end
     * @return array
     */
    public function findBookingsForWeek(string $start, string $end): array {
        $queryBuilder = $this->createQueryBuilder('b')
            ->select('b')
            ->where('b.date >= :start AND b.date <= :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end);
        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param Foodtruck $foodtruck
     * @return array
     */
    public function findBookingsForFoodTruck(Foodtruck $foodtruck): array {
        $queryBuilder = $this->createQueryBuilder('b')
            ->select('b')
            ->where('b.foodtruck = :foodtruck_id')
            ->setParameter('foodtruck_id', $foodtruck->getId());
        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param Foodtruck $foodtruck
     * @param $date
     * @return bool
     */
    public function hasBookingAlready(Foodtruck $foodtruck, $date): bool
    {
        $queryBuilder = $this->createQueryBuilder('b')
            ->select('b.id')
            ->where('b.foodtruck = :foodtruck_id and WEEK(b.date, 3) = WEEK(:date, 3)')
            ->setParameter('foodtruck_id', $foodtruck->getId())
            ->setParameter('date', $date);
        $results = $queryBuilder->getQuery()->getResult();
        return ($results && count($results) > 0);
    }

    /**
     * @param Foodtruck $foodtruck
     * @param int $bookingId
     * @return Booking|null
     */
    public function findBookingByIdForFoodtruck(Foodtruck $foodtruck, int $bookingId): ?Booking
    {
        return $this->findOneBy([
            'id' => $bookingId,
            'foodtruck' => $foodtruck->getId()
        ]);
    }
}
