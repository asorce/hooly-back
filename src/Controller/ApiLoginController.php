<?php

namespace App\Controller;

use App\Entity\Foodtruck;
use App\Helper\FoodtruckHelper;
use App\Repository\FoodtruckRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

/**
 * Watered down version of JWT on login
 * -> a simple token with an expiration date is generated to create a "session"
 * -> the token is passed upon every request to allow actions
 * -> the expiration date is checked as well
 *
 * Class ApiLoginController
 * @package App\Controller
 */
class ApiLoginController extends AbstractController
{
    public function __construct(private FoodtruckRepository $foodtruckRepository) {}

    #[Route('/api/login', name: 'api_login')]
    public function index(#[CurrentUser] UserInterface $foodtruck): Response
    {
        if (null === $foodtruck) {
            return $this->json([
                'message' => 'MISSING_CREDENTIALS',
            ], Response::HTTP_UNAUTHORIZED);
        }

        assert($foodtruck instanceof Foodtruck);
        try {
            $this->foodtruckRepository->createToken($foodtruck);
        } catch (\Exception $e) {
            return $this->json([
                'message' => 'CANNOT_LOGIN',
            ], Response::HTTP_UNAUTHORIZED);
        }

        $foodtruckHelper = new FoodtruckHelper(
            $foodtruck->getEmail(),
            $foodtruck->getApiToken(),
        );
        return $this->json($foodtruckHelper);
    }

    /*
    #[Route('/api/logout', name: 'api_logout', methods: ['GET'])]
    public function logoutAction(#[CurrentUser] UserInterface $foodtruck): Response
    {
        if (null !== $foodtruck) {
            assert($foodtruck instanceof Foodtruck);
            try {
                $this->foodtruckRepository->removeToken($foodtruck);
                $this->getUser()->
            } catch (\Exception $e) {
                return $this->json(['logout']);
            }
            return $this->json(['logout']);
        }
    }
    */
}
