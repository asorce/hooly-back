<?php

namespace App\Controller;

use App\Entity\Foodtruck;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\BookingService;

class BookingController extends AbstractController
{
    public function __construct(
        private BookingService $bookingService
    ) {}

    #[Route('/api/bookings', name: 'app_booking', methods: ['GET'])]
    public function list(): Response
    {
        $bookings = $this->bookingService->getBookingsForFoodtruck($this->getFoodtruck());
        return $this->json($bookings);
    }

    /**
     * To create a booking, a date is passed and the foodtruck is extracted from the token
     * -> A validator with a class could have been used to assert the values
     * -> For the purpose of this project, the date only might be enough
     * @param string $date
     * @return Response
     */
    #[Route('/api/booking/{date}', name: 'add_booking', methods: ['POST'])]
    public function create(string $date): Response
    {
        $bookingCreated = $this->bookingService->createBooking($this->getFoodtruck(), $date);
        return $this->json($bookingCreated, $bookingCreated ? Response::HTTP_CREATED : Response::HTTP_UNAUTHORIZED);
    }

    #[Route('/api/booking/{bookingId}', name: 'delete_booking', methods: ['DELETE'])]
    public function delete(int $bookingId): Response
    {
        $bookingDeleted = $this->bookingService->deleteBooking($this->getFoodtruck(), $bookingId);
        return $this->json($bookingDeleted, $bookingDeleted ? Response::HTTP_ACCEPTED : Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Helper function to retrieve the Foodtruck
     * @return Foodtruck|null
     */
    private function getFoodtruck(): ?Foodtruck {
        $foodtruck = $this->getUser();
        assert($foodtruck instanceof Foodtruck);
        return $foodtruck;
    }
}
