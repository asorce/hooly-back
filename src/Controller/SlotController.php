<?php

namespace App\Controller;

use App\Entity\Foodtruck;
use App\Service\BookingService;
use App\Service\SlotService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SlotController extends AbstractController
{
    public function __construct(
        private SlotService $slotService,
        private BookingService $bookingService,
    ) {}

    #[Route('/api/slots/', name: 'app_slot')]
    #[Route('/api/slots/{week}/{year}', name: 'app_slot_week')]
    public function index(?int $week, ?int $year): Response
    {
        if ($week && $year) {
            if ($week < 1 || $week > 52) {
                return $this->json([false], Response::HTTP_NOT_FOUND);
            }
            if ($year < 2022 || $year > 2030) {
                return $this->json([false], Response::HTTP_NOT_FOUND);
            }
        }
        $foodTruck = $this->getUser();
        assert($foodTruck instanceof Foodtruck);
        $slots = $this->slotService->getSlotsForWeek($week, $year);
        $bookings = $this->bookingService->getBookingsForWeek($foodTruck, array_key_first($slots), array_key_last($slots));
        $slotsHelper = $this->slotService->computeSlotsAndBookingsForWeek($slots, $bookings);
        return $this->json($slotsHelper);
    }
}
