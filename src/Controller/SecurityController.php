<?php

namespace App\Controller;

use App\Entity\Foodtruck;
use App\Repository\FoodtruckRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(private FoodtruckRepository $foodtruckRepository)
    {
    }

    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(#[CurrentUser] UserInterface $foodtruck): void
    {
        if (null !== $foodtruck) {

        assert($foodtruck instanceof Foodtruck);
            try {
                $this->foodtruckRepository->removeToken($foodtruck);
            } catch (\Exception $e) {
                // TODO: logger $e
            }
        }
    }
}
